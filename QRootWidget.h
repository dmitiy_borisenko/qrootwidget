#ifndef ROOT_WIDGET_H
#define ROOT_WIDGET_H

#include <QWidget>
#include <TCanvas.h>


class QRootWidget : public QWidget
{
   Q_OBJECT

public:
   QRootWidget( QWidget *parent = 0);
   virtual ~QRootWidget() {}

protected:
   TCanvas        *fCanvas;
};

#endif 