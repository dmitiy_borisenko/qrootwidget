QT          += widgets uiplugin

CONFIG      += plugin
INCLUDEPATH += $$system(root-config --incdir)
LIBS        += $$system(root-config --libs)
TEMPLATE    = lib

TARGET = $$qtLibraryTarget($$TARGET)

target.path = $$[QT_INSTALL_PLUGINS]/designer
INSTALLS += target

HEADERS     = QRootWidget.h \
              QRootWidgetPlugin.h
SOURCES     = QRootWidget.cpp \
              QRootWidgetPlugin.cpp