#include "QRootWidget.h"
#include "QRootWidgetPlugin.h"

#include <QtPlugin>

QRootWidgetPlugin::QRootWidgetPlugin(QObject *parent)
    : QObject(parent)
{
}

void QRootWidgetPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (initialized)
        return;

    initialized = true;
}

bool QRootWidgetPlugin::isInitialized() const
{
    return initialized;
}

QWidget *QRootWidgetPlugin::createWidget(QWidget *parent)
{
    return new QRootWidget(parent);
}

QString QRootWidgetPlugin::name() const
{
    return QStringLiteral("QRootWidget");
}

QString QRootWidgetPlugin::group() const
{
    return QStringLiteral("ROOT Features");
}

QIcon QRootWidgetPlugin::icon() const
{
    return QIcon();
}

QString QRootWidgetPlugin::toolTip() const
{
    return QString();
}

QString QRootWidgetPlugin::whatsThis() const
{
    return QString();
}

bool QRootWidgetPlugin::isContainer() const
{
    return false;
}

QString QRootWidgetPlugin::domXml() const
{
    return "<ui language=\"c++\">\n"
           " <widget class=\"QRootWidget\" name=\"QRootWidget\">\n"
           "  <property name=\"geometry\">\n"
           "   <rect>\n"
           "    <x>0</x>\n"
           "    <y>0</y>\n"
           "    <width>150</width>\n"
           "    <height>150</height>\n"
           "   </rect>\n"
           "  </property>\n"
           "  <property name=\"toolTip\" >\n"
           "   <string>TCanvas keeper</string>\n"
           "  </property>\n"
           "  <property name=\"whatsThis\" >\n"
           "   <string>Widget with embedded ROOT TCanvas.</string>\n"
           "  </property>\n"
           " </widget>\n"
           "</ui>\n";
}

QString QRootWidgetPlugin::includeFile() const
{
    return QStringLiteral("QRootWidget.h");
}