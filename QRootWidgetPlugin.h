#ifndef ROOT_WIDGET_PLUGIN_H
#define ROOT_WIDGET_PLUGIN_H

#include <QtUiPlugin/QDesignerCustomWidgetInterface>

class QRootWidgetPlugin : public QObject, public QDesignerCustomWidgetInterface
{
    Q_OBJECT

public:
    explicit QRootWidgetPlugin(QObject *parent = nullptr);

    bool isContainer() const override;
    bool isInitialized() const override;
    QIcon icon() const override;
    QString domXml() const override;
    QString group() const override;
    QString includeFile() const override;
    QString name() const override;
    QString toolTip() const override;
    QString whatsThis() const override;
    QWidget *createWidget(QWidget *parent) override;
    void initialize(QDesignerFormEditorInterface *core) override;

private:
    bool initialized = false;
};

#endif